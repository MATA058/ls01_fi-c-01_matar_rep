import java.util.ArrayList;

/**
 * Diese Klasse modelliert ein Raumschiff
 * 
 * @author Saumer Matar
 * @version 1.3 vom 25.04.2021
 */

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	// private ArrayList<String> logbuch= new ArrayList<String>();

	public Raumschiff() {
	}

	/**
	 * Vollparametrisierter Konstruktor für die Klasse Raumschiff
	 * 
	 * @param schiffsname
	 * @param photonentorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param zustandSchildInProzent
	 * @param zustandHuelleInProzent
	 * @param zustandLebenserhaltungssystemeInProzent
	 * @param androidenAnzahl
	 */

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, String schiffsname,
			int androidenAnzahl)

	{
		this.schiffsname = schiffsname;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.huelleInProzent = zustandHuelleInProzent;
		this.schildInProzent = zustandSchildInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	}

	// schiffsname

	/**
	 * Liefert den Namen des Schiffes
	 * 
	 * @return gibt den Namen des Schiffes zurück.
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;

	}
	// Energieversorgung

	public int energieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	// Photonentorpedo
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoanzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;

	}

	// Huelle
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;

	}

	// Schild
	public int getSchildInProzent() {
		return schildInProzent;
	}

	public void setSchildInProzent(int schildInProzent) {
		this.schildInProzent = schildInProzent;

	}

	// Androiden
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;

	}

	// Lebenserhaltungssystem
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;

	}

	//	neue Ladung
	public void beladen(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);

	}
	// Ladungsverzeichnis 
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Der Raumschiff " + this.schiffsname + " hat folgende Ladungen: ");
		for(Ladung temp: ladungsverzeichnis) {
			System.out.println(temp.getArt() + " " + temp.getMenge());
		}
	}
	// Nachrichten an alle schicken
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(this.schiffsname + ": " + message);
	}
	// Raumschiff wird getroffen
	private void treffer(Raumschiff r) {
		r.schildInProzent = r.schildInProzent - 50;
		if (r.schildInProzent <= 0) {
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}
		if (r.huelleInProzent <= 0) {
			r.lebenserhaltungssystemeInProzent = 0;
			r.nachrichtAnAlle ("Alle Lebenserhaltungssysteme wurde vernichtet");
		}
				
		System.out.println(r + " wurde getroffen");
	}
	// Zustand wird ausgegeben
	public void zustand() {
		System.out.println("Der momentane Zustand: ");
		System.out.println("Energie beträgt: "+ energieversorgungInProzent);
		System.out.println("Huelle beträgt: "+ huelleInProzent);
		System.out.println("Schild beträgt: "+ schildInProzent);
	}
	
}

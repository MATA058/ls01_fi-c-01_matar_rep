/**
 * Diese Klasse modeliert eine Ladung
 * 
 * @author Saumer Matar
 * @version 1.2 16.04.2021
 */

public class Ladung {

	private String art;
	private int menge;
//    private Raumschiff ladungsort;

	/**
	 * Vollparametersierte Konstruktor der Klasse Ladung, initialisiert alle
	 * Attribute
	 * 
	 * @param art   Die Art von der Ladung
	 * @param menge Die Menge von der Ladung
	 */

	public Ladung(String art, int menge) {
		this.art = art;
		this.menge = menge;

		/**
		 * Parameterlose Konstruktor der Klasse Ladung
		 */
	}

	public Ladung() {
	}

    public boolean getVerladen(){
        return verladen;
    }

    public void setVerladen(boolean verladen) {
        this.verladen = verladen;
    }

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return this.menge;
	}

	public String getArt() {
		return this.art;
	}

	public void setArt(String art) {
		this.art = art;
	}

    public Raumschiff getLadungsort() {
        return this.ladungsort;
    }

    public void setLadungsort(Raumschiff r) {
        this.ladungsort = r;
    }
}
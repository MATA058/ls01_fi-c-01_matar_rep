
public class TestWeltall {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Raumschiff k1 = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung ("Bat'leth Klingonen Schwer", 200);
		
		k1.beladen(l1);
		k1.beladen(l2);
		
		Raumschiff k2 = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Ladung l3 = new Ladung ("Borg-Schrott", 5);
		Ladung l4 = new Ladung ("Rote Materie", 2);
		Ladung l5 = new Ladung ("Plasma-Waffe", 50);
		
		k2.beladen(l3);
		k2.beladen(l4);
		k2.beladen(l5);
		
		Raumschiff k3 = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
		Ladung l6 = new Ladung ("Forschungssonde", 35);
		Ladung l7 = new Ladung ("Photonentorpedo", 3);
		
		k3.beladen(l6);
		k3.beladen(l7);

		k1.ladungsverzeichnisAusgeben();
		k2.ladungsverzeichnisAusgeben();
		k3.ladungsverzeichnisAusgeben();
		
	}

}



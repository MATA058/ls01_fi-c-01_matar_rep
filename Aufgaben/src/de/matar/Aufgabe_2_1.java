package de.matar;

public class Aufgabe_2_1 {

	public static void main(String[] args) {
		System.out.println("Fahrenheit  |  Celsius ");
		System.out.println("----------------------");
		System.out.println("-20	    |   28.89");
		System.out.println("-10	    |  -23.33");
		System.out.println("+0	    |  -17.78");
		System.out.println("+20	    |   -6.67");
		System.out.println("+30	    |   -1.11");
	}

}
